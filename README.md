# Тестовое задание #
#### Для проверки рекомендуется: ####
- Cкачать тестовое из этого репозитория.
- Установить виртуальное окружение.
- Скачать зависимости из `requirements.txt`
- Выполнить миграции.
- Проверить тестовое задание.

#### Образец заполнения файла `.env` ####
- SECRET_KEY=ключ проекта
- HOSTS='web localhost 127.0.0.1'
- DB_ENGINE=django.db.backends.postgresql
- DB_NAME=postgres 
- POSTGRES_USER=postgres
- POSTGRES_PASSWORD=postgres
- DB_HOST=db
- DB_PORT=5432 

#### Со всеми эндпоинтами можно ознакомится по адресу `docs/` ####

#### Развернуть проект с использованием docker-compose: ####
- Перейти в папку `infra/`
- Заполнить файл `.env` по образцу
- Поднять проект коммандой `docker-compose up`
- Создать миграции `docker-compose exec web python manage.py makemigrations`
- Выполнить миграции `docker-compose exec web python manage.py migrate`
- Собрать статику `docker-compose exec web python manage.py collectstic`

#### Документация: ####
- `api/v1/clients/{id}/` - Создание, изменение, удаление клиента из базы.
- `api/v1/distribution/{id}/` - Создание, просмотр, изменение, удаление рассылки из базы.

#### Дополнительное задание: ####
Выполнены пункты 3, 5
