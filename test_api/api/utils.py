from threading import Timer
import requests
from django.utils import timezone
from .models import Message, Client


NOW = timezone.now()
URL = 'https://probe.fbrq.cloud/v1/send/'
HEADERS = {"Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2OTQ3NzEyOTgsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6Imdvcl95YXIifQ.g_ydr_JBt2OOKtvSsrKDQyVS6GBwyYc-MQmJ8PkeruY"}


def message_creation(obj):
    text = obj.text
    filters = list(''.join(obj.filters).split())
    clients = Client.objects.all()
    for filter in filters:
        if filter.isdigit():
            clients = clients.filter(operators_code=filter)
        else:
            clients = clients.filter(tag=filter)
    phone_numbers = []
    for client in clients:
        phone_numbers.append(client.phone_number)
    message_data = {}
    if phone_numbers:
        for phone_number in phone_numbers:
            if not Message.objects.filter(
                client_id = Client.objects.get(phone_number=phone_number).id,
                dist_id = obj.id
            ).exists():
                message_obj = Message.objects.create(
                    date_create = NOW,
                    status_dist = 'Created',
                    client_id = Client.objects.get(phone_number=phone_number).id,
                    dist_id = obj.id
                )
                message_data[message_obj.id] = phone_number
    if obj.start_time >= NOW and obj.end_time < NOW:
        message_distribution(text, message_data)
    elif obj.start_time > NOW:
        Timer(obj.start_time - NOW, message_distribution(text, message_data))
    return None


def message_distribution(text, message_data):
    for id, phone_number in message_data.items():
        if Message.objects.get(id=id).status_dist != 'Sent':
            data = {
                "id": id,
                "phone": phone_number,
                "text": text
            }
            new_url = f'{URL}{id}'
            try:
                requests.post(url=new_url, data=data, headers=HEADERS)
                Message.objects.filter(id=id).update(status_dist='Sent')
            except Exception:
                Message.objects.filter(id=id).update(status_dist='Error')
                message_distribution(text, message_data)
    return None


