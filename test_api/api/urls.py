from django.urls import include, path
from rest_framework import routers

from .views import ClientViewSet, DistributionViewSet

router_v1 = routers.DefaultRouter()
router_v1.register('clients', ClientViewSet)
router_v1.register('distribution', DistributionViewSet)

urlpatterns = [
    path('', include(router_v1.urls)),
]
