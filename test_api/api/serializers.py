from rest_framework import serializers
from .models import Message, Distribution, Client


class ClientSerializer(serializers.ModelSerializer):
    phone_number = serializers.CharField(required=True)
    operators_code = serializers.IntegerField(required=True)
    timezone = serializers.CharField(required=True)

    class Meta:
        model = Client
        fields = (
            'id',
            'phone_number',
            'operators_code',
            'tag',
            'timezone'
        )
        read_only_fields = (
            'id',
        )


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = (
            'id',
            'date_create',
            'status_dist',
            'dist',
            'client',
        )
        read_only_fields = (
            'id',
            'date_create',
            'status_dist',
            'dist',
            'client',
        )


class DistributionSerializer(serializers.ModelSerializer):
    start_time = serializers.DateTimeField(required=True)
    text = serializers.CharField(required=True)
    end_time = serializers.DateTimeField(required=True)
    filters = serializers.CharField()
    messages = serializers.SerializerMethodField()
    messages_count = serializers.SerializerMethodField()
    class Meta:
        model = Distribution
        fields = (
            'id',
            'start_time',
            'text',
            'filters',
            'end_time',
            'messages',
            'messages_count'
        )
        read_only_fields = (
            'id',
        )

    def get_messages_count(self, obj):
        return Message.objects.filter(dist=obj).count()
    
    def get_messages(self, obj):
        return Message.objects.filter(dist=obj)
    
    def to_representation(self, instance):
        data = super().to_representation(instance)
        if data['messages']:
            messages = data.pop('messages')
            data['messages'] = []
            for message in messages:
                data['messages'].append(MessageSerializer(message).data)
        return data
        


