from rest_framework import viewsets
from .serializers import ClientSerializer, DistributionSerializer
from .models import Client, Distribution
from .mixins import CreateUpdateDeleteViewSet
from .utils import message_creation


class ClientViewSet(CreateUpdateDeleteViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
    lookup_field = 'id'

class DistributionViewSet(viewsets.ModelViewSet):
    queryset = Distribution.objects.all()
    serializer_class = DistributionSerializer
    lookup_field = 'id'

    def create(self, request, *args, **kwargs):
        data = super().create(request, *args, **kwargs)
        message_creation(Distribution.objects.get(id=data.data['id']))
        return data
    
    def update(self, request, *args, **kwargs):
        data = super().update(request, *args, **kwargs)
        message_creation(Distribution.objects.get(id=data.data['id']))
        return data

