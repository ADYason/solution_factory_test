from django.core.validators import RegexValidator
from django.contrib.auth import get_user_model
from django.db import models

User = get_user_model()


class Client(models.Model):
    phone_regex = RegexValidator(
        regex=r'^\+?1?\d{9,12}$',
        message="Номер телефона должен быть в формате: '7XXXXXXXXXX' (X - цифра от 0 до 9)"
    )
    phone_number = models.CharField(
        validators=[phone_regex],
        max_length=12,
        blank=False
    )
    operators_code = models.IntegerField()
    tag = models.CharField(max_length=10,)
    timezone = models.CharField(max_length=10,)

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'

    def __str__(self):
        return self.phone_number


class Distribution(models.Model):
    start_time = models.DateTimeField()
    text = models.TextField()
    filters = models.TextField(default=None)
    end_time = models.DateTimeField()

    class Meta:
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'

    def __str__(self):
        return self.text


class Message(models.Model):
    date_create = models.DateTimeField(auto_now_add=True,)
    status_dist = models.CharField(max_length=10)
    dist = models.ForeignKey(
        Distribution,
        on_delete=models.CASCADE,
        related_name='message'
    )
    client = models.ForeignKey(
        Client,
        on_delete=models.CASCADE,
        related_name='message'
    )

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'
        ordering = ['status_dist']
        constraints = [
            models.UniqueConstraint(fields=['dist', 'client'],
                                    name='message_unique')
        ]
