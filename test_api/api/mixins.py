from rest_framework import mixins, viewsets, permissions


class CreateUpdateDeleteViewSet(mixins.CreateModelMixin,
                                mixins.UpdateModelMixin,
                                mixins.DestroyModelMixin,
                                viewsets.GenericViewSet):
    permission_classes = (
        permissions.AllowAny,
    )
    lookup_field = 'id'
