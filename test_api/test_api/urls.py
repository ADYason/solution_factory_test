from django.urls import path, include
from django.conf.urls import url
from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='Documentation')

urlpatterns = [
    path('api/v1/', include(('api.urls', 'api'), namespace='api')),
    url('docs/', schema_view),
]
